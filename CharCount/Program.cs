﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharCount
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input city name:");
            Console.WriteLine(new StringExtension<string>().StringCount(Console.ReadLine()).TextResult);
            Console.ReadLine();
        }
    }
    public  class StringExtension<T> {
        public string TextResult { get; set; }
        public StringExtension<T> StringCount(T paras)
        {
            foreach (var chr in paras.ToString().ToLower().GroupBy(g => g)) {
                TextResult += chr.Key.ToString() + " : "+chr.Count().ToString()+"\n";
            }
            return this;
        }
  
    }
}
